{{- if .Values.ingress.enabled -}}
Application should be accessible at: {{ .Values.ingress.url }}
{{- if ne (len .Values.ingress.additionalHosts) 0 -}}
And additional on:
{{- range $url := .Values.ingress.additionalHosts -}}
- {{ $url }}
{{- end -}}
{{- end -}}
{{- else -}}
Application will be accessible at: {{ .Values.ingress.url }} when you enable .Values.ingress.enabled
{{- end -}}
{{- if .Values.service.enabled }}
Application internal address: {{ template "fullname" . }}.{{ .Release.Namespace }}
{{- end -}}
